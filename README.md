# helpre

This little tool aids extracting tag information from your changelog. It is
designed to be used in CI environment like the GitLab CI where this is one of
the missing features for automated releases.

## Usage

The `--help` says most things, so let's start with that

```text
helpre 0.1.0
Island of TeX
Output details from a CHANGELOG.md

USAGE:
    helpre [FLAGS] [OPTIONS] <input-file>

ARGS:
    <input-file>    The input file to be parsed. Usually, you will want to use `CHANGELOG.md`
                    here

FLAGS:
        --help        Prints help information
    -o, --only-tag    Show only the tag of the latest release (ignoring unreleased sections). Do not
                      print the description
    -V, --version     Prints version information

OPTIONS:
    -h, --heading-capture <heading-capture>
            The regular expression that will be used on the heading's text to split off possible
            dates and such. Please specify a capture group. Currently, only splitting off at the end
            is supported [default: \[*([A-Za-z0-9\.-]+)\]*(\s-\s.*)*]

    -i, --ignore-headings <ignore-headings>...
            Ignore certain headings like `Unreleased`. That value will be used by default but can be
            overridden

    -t, --target-heading-level <target-heading-level>
            Set the target heading level, defaults to 2, i.e. `##` [default: 2]
```

Say you have the following CHANGELOG:

```markdown
# Changelog

## Unreleased

Such a missing feature…

## v1.0.0

Initial release
```

You can do the following using `helpre`:

* extract the latest tag using `helpre -o CHANGELOG` which will give you
  `v1.0.0`. `Unreleased` is ignored by default.
* extract the release text using `helpre CHANGELOG` which will output
    ```markdown
	## v1.0.0
	
	Initial release
	```

Using the options you may even exclude other heading than just `Unreleased` and
change the target heading level.

Important note: The default regular expression to match tags is oriented follows
the [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) format, so users of
this format will have the best out-of-the-box experience.

## The name

Naming things is one of the most difficult things. What looks like a typo is
actually a shorthand for "help releasing" because it's the intended use case.
