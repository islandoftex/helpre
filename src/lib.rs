use regex::Regex;
use std::collections::HashSet;

/// Options to configure the extraction of tags from headings.
pub struct ExtractionOptions<'a> {
    /// The target heading level, i.e. the number of `#` characters.
    pub target_heading_level: u32,
    /// Which targets to skip, i.e. which tags. Lookup is performed
    /// after applying `heading_regex`.
    pub skip_targets: &'a HashSet<String>,
    /// Regex to match heading texts against. The preceding `#` are
    /// already stripped. This must contain at least one match group
    /// which is used as the tag.
    pub heading_regex: &'a Regex,
}

/// Match a certain tag (i.e. markdown heading in the changelog).
pub struct TagMatch<'a> {
    /// The tag's content after extracting it with the regular
    /// expression from `ExtractionOptions`.
    pub tag_content: &'a str,
    /// The start of the tag's heading. This is before the first
    /// `#` character.
    pub start: usize,
    /// The end of the tag's heading, i.e. the newline after the
    /// heading.
    pub end: usize,
}

/// Extract the latest tag from `base` using the given `options`.
/// If there is no matching heading, returns `None`.
pub fn extract_tag<'a>(base: &'a str, options: &ExtractionOptions) -> Option<TagMatch<'a>> {
    let heading_level = options.target_heading_level as usize;
    let heading_start = format!("{} ", (0..heading_level).map(|_| '#').collect::<String>());

    let mut pos_match = base.find(&heading_start);
    while let Some(pos) = pos_match {
        let next_line = base[pos..].find('\n').map(|l| pos + l);

        if next_line.is_none() || (pos > 0 && &base[pos - 1..pos] == "#") {
            let next = pos + heading_level;
            pos_match = base[next..].find(&heading_start).map(|x| next + x);
            continue;
        }
        let next_line = next_line.unwrap();

        let tag_string = options
            .heading_regex
            .captures(&base[pos..next_line])
            .and_then(|capt| capt.get(1))
            .map(|heading| heading.as_str());
        if let Some(s) = tag_string {
            if !options.skip_targets.contains(s) {
                return Some(TagMatch {
                    tag_content: s,
                    start: pos,
                    end: next_line,
                });
            }
        }

        pos_match = base[next_line..]
            .find(&heading_start)
            .map(|x| next_line + x);
    }

    None
}

#[derive(Debug, Clone)]
pub struct HelpreError(pub &'static str);

impl std::fmt::Display for HelpreError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn with_default_options<R, F>(consumer: F) -> R
    where
        F: FnOnce(&ExtractionOptions) -> R,
    {
        let regex = Regex::new("\\[*([A-Za-z0-9\\.-]+)\\]*(\\s-\\s.*)*").unwrap();
        let mut skip_targets = HashSet::with_capacity(1);
        skip_targets.insert(String::from("Unreleased"));
        let options = ExtractionOptions {
            target_heading_level: 2,
            skip_targets: &skip_targets,
            heading_regex: &regex,
        };
        consumer(&options)
    }

    #[test]
    fn find_heading_at_start() {
        let target = "## Test\n";

        with_default_options(|options| match extract_tag(target, &options) {
            Some(tag) => {
                assert_eq!("Test", tag.tag_content);
                assert_eq!(0, tag.start);
                assert_eq!(target.len() - 1, tag.end);
            }
            None => assert!(false),
        });
    }

    #[test]
    fn find_heading_at_end() {
        let target = "Line\nLine2\n\n\nLine3\n## Test\n";

        with_default_options(|options| match extract_tag(target, &options) {
            Some(tag) => {
                assert_eq!("Test", tag.tag_content);
                assert_eq!(19, tag.start);
                assert_eq!(target.len() - 1, tag.end);
            }
            None => assert!(false),
        });
    }

    #[test]
    fn skip_wrong_levels() {
        let target = "#### A\n### B\n# C\nD\n## Test\n";

        with_default_options(|options| match extract_tag(target, &options) {
            Some(tag) => {
                assert_eq!("Test", tag.tag_content);
                assert_eq!(target.len() - 1, tag.end);
            }
            None => assert!(false),
        });
    }

    #[test]
    fn skip_ignore_keywords() {
        let target = "## Unreleased\n## Test\n";

        with_default_options(|options| match extract_tag(target, &options) {
            Some(tag) => {
                assert_eq!("Test", tag.tag_content);
                assert_eq!(target.len() - 1, tag.end);
            }
            None => assert!(false),
        });
    }

    #[test]
    fn match_keepachangelog_tag() {
        let target = "## [Test] - 1970-01-01\n";

        with_default_options(|options| match extract_tag(target, &options) {
            Some(tag) => {
                assert_eq!("Test", tag.tag_content);
                assert_eq!(target.len() - 1, tag.end);
            }
            None => assert!(false),
        });
    }

    #[test]
    fn fail_to_find_tag_in_empty_target() {
        let target = "";
        with_default_options(|options| assert!(extract_tag(target, &options).is_none()));
    }
}
