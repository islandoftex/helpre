use clap::{crate_version, Clap};
use helpre::{extract_tag, ExtractionOptions, HelpreError};
use regex::Regex;
use std::collections::HashSet;

/// Output details from a CHANGELOG.md
#[derive(Clap)]
#[clap(version = crate_version!(), author = "Island of TeX")]
struct CLIArgs {
    /// The input file to be parsed. Usually, you will want to use
    /// `CHANGELOG.md` here.
    input_file: String,
    /// Show only the tag of the latest release (ignoring unreleased
    /// sections). Do not print the description.
    #[clap(short, long)]
    only_tag: bool,
    /// Ignore certain headings like `Unreleased`. That value will be
    /// used by default but can be overridden.
    #[clap(short, long, multiple = true)]
    ignore_headings: Vec<String>,
    /// Set the target heading level, defaults to 2, i.e. `##`.
    #[clap(short, long, default_value = "2")]
    target_heading_level: u32,
    /// The regular expression that will be used on the heading's
    /// text to split off possible dates and such. Please specify
    /// a capture group. Currently, only splitting off at the end
    /// is supported.
    #[clap(short, long, default_value = "\\[*([A-Za-z0-9\\.-]+)\\]*(\\s-\\s.*)*")]
    heading_capture: String,
}

fn main() -> Result<(), HelpreError> {
    let args = CLIArgs::parse();
    let heading_regex = match Regex::new(&args.heading_capture) {
        Ok(x) => Ok(x),
        Err(_) => Err(HelpreError("Your regex could not be validated.")),
    }?;

    let markdown_input = match std::fs::read_to_string(args.input_file) {
        Ok(x) => Ok(x),
        Err(_) => Err(HelpreError("Reading the input file failed")),
    }?;

    let mut skip_targets = HashSet::with_capacity(args.ignore_headings.len() + 1);
    if args.ignore_headings.is_empty() {
        skip_targets.insert(String::from("Unreleased"));
    } else {
        for i in args.ignore_headings {
            skip_targets.insert(i);
        }
    }

    let extraction_options = ExtractionOptions {
        target_heading_level: args.target_heading_level,
        skip_targets: &skip_targets,
        heading_regex: &heading_regex,
    };

    let tag = extract_tag(&markdown_input, &extraction_options);
    if args.only_tag {
        match tag {
            Some(s) => println!("{}", s.tag_content),
            None => eprintln!("No tag found in markdown"),
        }
        return Ok(());
    }

    let tag_position = match &tag {
        Some(s) => Ok(s.start),
        None => Err(HelpreError("No tag found in markdown")),
    }?;

    skip_targets.insert(tag.unwrap().tag_content.to_string());
    let extraction_options = ExtractionOptions {
        target_heading_level: args.target_heading_level,
        skip_targets: &skip_targets,
        heading_regex: &heading_regex,
    };
    let extracted_tag = extract_tag(&markdown_input, &extraction_options);
    let next_position = match &extracted_tag {
        Some(s) => Ok(s.start - 1),
        None => Ok(markdown_input.len()),
    }?;

    println!("{}", &markdown_input[tag_position..next_position]);

    Ok(())
}
